defmodule Scheduler.MixProject do
  use Mix.Project

  def project do
    [
      app: :scheduler,
      version: "0.1.0",
      elixir: "~> 1.14",
      start_permanent: Mix.env() == :prod,
      docs: [
        main: "Scheduler"
      ],
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger,:tools]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:treex, "~> 0.1.0"},
      {:timex, "~> 3.7.11"},
      {:ex_doc, "~> 0.31", only: :dev, runtime: false}
    ]
  end
end
