# scheduler

This is an Elixir library for exercising precise control over the scheduling of conflicting
actions in a concurrent Elixir program.

