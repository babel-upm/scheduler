defmodule Scheduler.Explore do

  @moduledoc """
  Provides function for the systematic exploration of schedules.
  """

  require Logger

  @doc "Explores a scenario using a random (but with the scheduler enabled) exploration"
  def explore_random(scenario), do: explore_random(scenario,[])
  def explore_random(scenario,options) do
    results = [{:results,[]},{:start_time,Timex.now()} | options]
    explore_random1(scenario,results)
  end

  def explore_random1(scenario,results) do
    {_,result} = run_scenario(scenario,:random)
    results = upd(results,result)
    if check_termination(results) do
      results
    else
      explore_random1(scenario,results)
    end
  end

  @doc "Explores a scenario using a dept-first exploration"
  def explore_depth_first(scenario), do: explore_depth_first(scenario,[])
  def explore_depth_first(scenario,options) do
    results = [{:results,[]},{:start_time,Timex.now()} | options]
    {state,result} = run_scenario(scenario,:random)
    Logger.info("#{inspect(Enum.reverse(state.history),pretty: true)}")
    results = upd(results,result)
    if check_termination(results) do
      results
    else
      explore_trace(scenario,state.history,results)
    end
  end

  defp explore_trace(scenario,trace,results) do
    Logger.info(fn () -> "Trace to explore is\n#{inspect(trace,pretty: true)}" end)
    case search_for_choice_point(trace) do
      {trace_to_run, trace_to_explore} ->

        # committed to that choice point, with no earlier choice points
        Logger.info(fn () -> "#{inspect(trace_to_run, pretty: true)}" end)
        {state,result} = run_scenario(scenario,{:follow,trace_to_run})
        results = upd(results,result)
        if check_termination(results) do
          results
        else
          results = explore_trace(scenario,state.followed_trace,results)
          if check_termination(results) do
            results
          else
            # choice point removed, but with earlier choice points
            explore_trace(scenario,trace_to_explore,results)
          end
        end

      nil -> results
    end
  end

  @doc "Explores a scenario using a controlled random exploration (which avoids repeating schedules)"
  def explore_controlled_random(scenario), do: explore_controlled_random(scenario,[])
  def explore_controlled_random(scenario,options) do
    results = [{:results,[]},{:start_time,Timex.now()} | options]
    {state,result} = run_scenario(scenario,:random)
    results = upd(results,result)
    if check_termination(results) do
      results
    else
      history = Enum.reverse(state.history)
      alternatives = all_alternatives(history)
      table = Treex.empty()
      table = add_alternatives(table,alternatives)
      explore_controlled_random1(scenario,table,results)
    end
  end

  def explore_controlled_random1(scenario,table,results) do
    case random_key(table) do
      nil ->
        results
      trace ->
        table = Treex.delete!(table,trace)
        Logger.info(fn () -> "will follow trace #{inspect trace}" end)
        {state,result} = run_scenario(scenario,{:follow,trace})
        Logger.info(fn () -> "followed trace #{inspect state.followed_trace}" end)
        results = upd(results,result)
        if check_termination(results) do
          results
        else
          alternatives = all_alternatives(Enum.reverse(state.followed_trace))
          table = add_alternatives(table,alternatives)
          explore_controlled_random1(scenario,table,results)
        end
    end
  end

  defp random_key(table) do
    {size, tree} = table
    if size > 0 do
      random_key(tree,size)
    else nil
    end
  end
  defp random_key(tree,size) do
    case tree do
      {key,_,nil,nil} -> key
      {key,_,nil,right} ->
        case :rand.uniform(size) do
          1 -> key
          _ -> random_key(right,size-1)
        end
      {key,_,left,nil} ->
        case :rand.uniform(size) do
          1 -> key
          _ -> random_key(left,size-1)
        end
        {key,_,left,right} ->
        rnd = :rand.uniform(size)
        cond do
          rnd == 1 -> key
          rem(rnd,2) == 0 -> random_key(right,div(size,2))
          true -> random_key(left,div(size,2))
        end
    end
  end

  defp all_alternatives(history), do: all_alternatives(history,[])
  defp all_alternatives([],_prefix), do: []
  defp all_alternatives([{location,[],result}|rest],prefix) do
    all_alternatives(rest,[{location,result}|prefix])
  end
  defp all_alternatives([{location,alternatives,result}|rest],prefix) do
    alternatives = Enum.map(alternatives, fn alternative -> Enum.reverse(prefix,[{alternative,:not_run}]) end)
    rest_alternatives = all_alternatives(rest,[{location,result}|prefix])
    alternatives++rest_alternatives
  end

  defp add_alternatives(table,alternatives) do
    Enum.reduce(alternatives, table, fn (alternative,table) ->
      Treex.enter(table,alternative,0)
    end)
  end

  def run_program(program) do
    run_scenario(program,:random)
  end

  def run_scenario(program,scheduler) when is_function(program) do
    run_scenario([execute: fn (_) -> program.() end],scheduler)
  end
  def run_scenario(scenario,scheduler) do
    {:ok,_} = Scheduler.start_link()
    case Keyword.get(scenario,:startup) do
      nil -> :ok
      f -> f.(scenario)
    end
    Logger.info(fn () -> "scheduler is #{inspect scheduler}" end)
    Scheduler.start_scheduler(scheduler)
    execute = Keyword.get(scenario,:execute)
    tasks = execute.(scenario)
    result = Task.await_many(tasks,50000)
    Logger.info(fn () -> "result is #{inspect result}" end)
    scheduler_state = Scheduler.stop()
    Process.sleep(100)
    stopping_result = case Keyword.get(scenario,:stopping) do  # stop_monitoring()
      nil -> result
      f -> f.(scenario,scheduler_state,result)
    end
    {scheduler_state,stopping_result}
  end

  defp search_for_choice_point([]), do: nil
  defp search_for_choice_point([ {_,[],_} | rest ]), do: search_for_choice_point(rest)
  defp search_for_choice_point([ {_,[first_cp|rest_cps],_} | rest ]) do
    first_to_run = remove_choicepoints(rest) |> Enum.reverse([{first_cp,:not_run}])
    next_to_explore = [{first_cp,rest_cps,:not_run}|rest]
    Logger.info(fn () -> "first to explore is\n#{inspect(first_to_run,pretty: true)}" end)
    Logger.info(fn () -> "next to explore is\n#{inspect(next_to_explore,pretty: true)}" end)
    {first_to_run,next_to_explore}
  end

  defp remove_choicepoints(trace) do
    Enum.map(trace, fn ({choice,_,result}) -> {choice,result} end)
  end

  defp upd(old_results,result) do
    check_results = Keyword.get(old_results,:results)
    n = case List.keyfind(check_results,result,0) do
                nil -> 1
                {_,n} -> n+1
              end
    check_results = List.keystore(check_results,result,0,{result,n})
    results = List.keystore(old_results,:results,0,{:results,check_results})
    results = case Keyword.get(results,:counter) do
                nil -> results
                n -> List.keystore(results,:counter,0,{:counter,n-1})
              end
    results
  end

  def check_termination(config) do
    stop_on_result = Keyword.get(config,:stop_on_result)
    results = Keyword.get(config,:results)
    timeout = Keyword.get(config,:timeout)
    counter = Keyword.get(config,:counter)
    cond do
      is_function(stop_on_result) and stop_on_result.(results) ->
        true
      timeout && Timex.compare(timeout,Timex.now()) <= 0 ->
        true
      counter ->
        counter <= 0
      true ->
        false
    end
  end

end
