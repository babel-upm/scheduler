defmodule Scheduler.Examples.Snapshot.Scenario do

  require Logger

  def exec_scenario(options) do
    module = Keyword.get(options,:module)
    n_scanners = Keyword.get(options,:n_scanners)
    n_writers = Keyword.get(options,:n_writers)
    array_size = Keyword.get(options,:array_size)
    writer_rounds = Keyword.get(options,:writer_rounds)
    scanner_rounds = Keyword.get(options,:scanner_rounds)
    after_setup = Keyword.get(options,:after_setup,nil)

    Scheduler.register_process_name(:create)
    module.create(array_size)

    if after_setup, do: after_setup.()

    Logger.info("will start snapshot tasks")

    {map,_} =
      Enum.reduce(1..n_writers, {%{},1}, fn (w,{map,counter}) ->
        Enum.reduce(0..array_size-1, {map,counter}, fn (a,{map,counter}) ->
          Enum.reduce(1..writer_rounds, {map,counter}, fn (r,{map,counter}) ->
            {Map.put(map,{w,a,r},counter),counter+1}
          end)
        end)
      end)
            

    writer_tasks =
      Enum.flat_map(1..n_writers, fn w ->
        Enum.map(0..array_size-1, fn a ->
          Task.async(fn () ->
            Scheduler.register_process_name({:writer,w,a})
            for r <- 1..writer_rounds do
              v = Map.get(map,{w,a,r})
              module.write(a,v)
              Logger.info(fn () -> "#{inspect Time.to_iso8601(Time.utc_now)}: writer #{inspect v} done" end)
            end
          end)
        end)
      end)

    scanner_tasks = Enum.map(1..n_scanners, fn s ->
      Task.async(fn () ->
        Scheduler.register_process_name({:scanner,s})
        for _i <- 1..scanner_rounds do
          module.scan(array_size)
          Logger.info(fn () -> "#{inspect Time.to_iso8601(Time.utc_now)}: scanner done" end)
        end
      end)
    end)

    writer_tasks ++ scanner_tasks
  end

end
