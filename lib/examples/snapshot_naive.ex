defmodule Scheduler.Examples.Snapshot.Naive do

  alias Scheduler.Examples.EtsE

  use Scheduler, modules: [EtsE]

  def create(size) do
    EtsE.new()
    0..size-1
    |> Enum.each(fn i -> EtsE.put(A,i,0) end)
  end

  def write(i,v) do
    EtsE.put(A,i,v)
  end

  def scan(size) do
    Enum.map(0..size-1,fn i ->
      EtsE.get(A,i)
    end)
    |> List.to_tuple
  end

end

