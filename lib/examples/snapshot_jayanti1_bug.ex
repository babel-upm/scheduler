defmodule Scheduler.Examples.Snapshot.Jayanti1Bug do

  alias Scheduler.Examples.EtsE

  use Scheduler, modules: [EtsE]

  def create(size) do
    EtsE.new()
    Enum.each(0..size-1,fn i ->
      EtsE.put(A,i,0)
      EtsE.put(B,i,0)
    end)
    EtsE.put(X,false)
  end

  def write(i,v) do
    EtsE.put(A,i,v)
    if EtsE.get(X) do
      EtsE.put(B,i,v)
    end 
  end

  def scan(size) do
    Enum.each(0..size-1,fn i ->
      EtsE.put(B,i,:bottom)
    end)
    EtsE.put(X,true)
    v = Enum.map(0..size-1, fn i ->
      {i,EtsE.get(A,i)}
    end)
    EtsE.put(X,false) 
    v = Enum.map(v, fn {i,vv} ->
      b = EtsE.get(B,i)
      if b != :bottom, do: b, else: vv
    end)
    List.to_tuple(v)
  end

end
