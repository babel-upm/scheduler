defmodule Scheduler.Examples.EtsE do

  def new() do
    :ets.new(Sntab,[:set,:named_table,:public])
  end

  def delete() do
    :ets.delete(Sntab)
  end

  def get(key) do
    case :ets.lookup(Sntab,key) do
      [{_,value}] ->
        value
      [] ->
        raise("EtsE.get(#{inspect key}) cannot find key")
    end
  end

  def put(key,value) do
    :ets.insert(Sntab,{key,value})
  end

  def get(key,index) do
    case :ets.lookup(Sntab,{key,index}) do
      [{_,value}] ->
        value
      [] ->
        raise("EtsE.get(#{inspect key},#{inspect index}) cannot find key or index")
    end
  end

  def put(key,index,value) do
    :ets.insert(Sntab,{{key,index},value})
  end

end
