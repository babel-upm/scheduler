defmodule Scheduler.Examples.TestSend do

  use Scheduler, modules: [Process], send: true, receive: true #, debug: true
  alias Scheduler.Explore
  require Logger

  def starter() do
    starter = 
      Task.async(fn ()->
        Scheduler.register_process_name(:starter)
        receive do
          result -> result
        end
      end)

    receiver =
      Task.async(fn () ->
        Scheduler.register_process_name(:receiver)
        receiver(starter.pid,2)
      end)
    
    sender1 =
      Task.async(fn () ->
        Scheduler.register_process_name(:sender1)
        Process.register(self(),:hi)
        send(receiver.pid,"1")
      end)

    sender2 =
      Task.async(fn () ->
        Scheduler.register_process_name(:sender2)
        send(receiver.pid,"2")
      end)

    [starter, receiver, sender1, sender2]
  end

  def receiver(reporter,n), do: receiver(reporter,n,[])
  def receiver(reporter,0,result), do: send(reporter,Enum.reverse(result))
  def receiver(reporter,n,result) do
    receive do
      msg -> receiver(reporter,n-1,[msg|result])
      after 10000 -> send(reporter,:timeout)
    end
  end

  def explore_depth_first() do
    Logger.configure(level: :error)
    Explore.explore_depth_first(&__MODULE__.starter/0)
  end

  def explore_controlled_random() do
    Logger.configure(level: :error)
    Explore.explore_controlled_random(&__MODULE__.starter/0)
  end

  def explore_random() do
    Logger.configure(level: :error)
    Explore.explore_random(&__MODULE__.starter/0,[counter: 100])
  end

  def explore_interactive() do
    Explore.run_scenario(&__MODULE__.starter/0,:interactive)
  end

  def explore_follow_interactive() do
    {state, _} = Explore.run_scenario(&__MODULE__.starter/0,:random)
    trace =
      Enum.reverse(state.history)
      |> Enum.map(fn {call,_} -> call end)
    follow_trace = case trace do
                     [e1,e2,_|_] -> [e1,e2]
                     _ -> state.trace
                   end
    Explore.run_scenario(&__MODULE__.starter/0,{:follow_interactive,follow_trace})
  end
  
end
