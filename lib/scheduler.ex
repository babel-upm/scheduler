defmodule Scheduler do
  use GenServer
  require Logger
  
  @moduledoc """
  Provides macros and functions to run a program under control over this user scheduler.
  """

  @timeout_random 2
  @timeout_following 10000
  @timeout_interactive 200
  @timeout_no_activity :infinity
  
  defmacro __before_compile__(_env) do
    quote do
    end
  end
  
  defmacro __using__(options) do
    Module.register_attribute(__CALLER__.module, :module, accumulate: true)
    Module.register_attribute(__CALLER__.module, :modules, [])
    Module.register_attribute(__CALLER__.module, :options, [])
    
    configure_module(options, __CALLER__)
    
    quote do
      import Kernel, except: [def: 2, def: 1, defp: 2, defp: 1]
      import unquote(__MODULE__)
      require unquote(__MODULE__)
      @before_compile unquote(__MODULE__)
    end
  end
  
  defmacro def(call, expr \\ nil), do: instrument_fun_def(:def, __CALLER__.module, call, expr)
  defmacro defp(call, expr \\ nil), do: instrument_fun_def(:defp, __CALLER__.module, call, expr)

  defp instrument_fun_def(fun, module, call, expr) do
    modules = Module.get_attribute(module, :modules)
    options = Module.get_attribute(module, :options)
    
    {name,_,args} = call
    [do: do_body] = expr
    {_,should_instrument} = should_instrument(modules,options,do_body)
    
    do_body = if should_instrument do
      body = instrument_body(do_body,name,args,modules,options)
      if Keyword.get(options,:debug,false) do
        IO.puts("Code generated for #{inspect name}:\n#{Macro.to_string(body)}")
      end
      body
    else
      do_body
    end
    
    quote do
      Kernel.unquote(fun)(unquote(call), unquote([do: do_body]))
    end
  end

  defp instrument_body(do_body, name, args, modules, options) do
    offline_register = if Keyword.get(options,:offline,false) do
      quote do
        [
          Stateful.OfflineMonitor.register_monitored_call({:call,__MODULE__,unquote(name),unquote(args)})
        ]
      end
    else []
    end
    
    offline_maybe_start = if Keyword.get(options,:offline,false) do
      quote do
        [
          Stateful.OfflineMonitor.maybe_start_monitored_call()
        ]
      end
    else []
    end
    
    offline_finish = if Keyword.get(options,:offline,false) do
      quote do
        [
        if Stateful.OfflineMonitor.should_finish() do
          Scheduler.permission({:finishing,unquote(__MODULE__),unquote(name),unquote(args)})
          Scheduler.stopped(result)
        end,
        Stateful.OfflineMonitor.finish_monitored_call(result),
        ]
      end
      else []
    end

    {do_body,_} = 
      Macro.postwalk(do_body, 0, fn (ast,acc) ->
        case ast do
          {{:.,line1,[{:__aliases__,line2,[module]},fun]},line3,args}
            ->
            if Enum.member?(modules,module) do
              {assign_vars,new_args} = eval_args(args)
              ast = {{:.,line1,[{:__aliases__,line2,[module]},fun]},line3,new_args}
              expr = execute_ast_with_permission(ast,offline_maybe_start)
              body = quote do
                (
                  unquote_splicing(assign_vars)
                  Scheduler.permission({unquote(fun),unquote(acc),unquote(line1)}) ;
                  unquote(expr)
              )
              end
              {body,acc+1}
            else {ast,acc}
            end
          {:send, line, args=[ _, _]} ->
            if Keyword.get(options,:send,false) do
              {assign_vars,new_args} = eval_args(args)
              ast = {:send, line, new_args}
              expr = execute_ast_with_permission(ast,offline_maybe_start)
              body = quote do
                (
                  unquote_splicing(assign_vars)
                  Scheduler.permission({:send,unquote(acc),unquote(line)})
                  unquote(expr)
                )
              end
              {body,acc+1}
            else {ast,acc}
            end
          {:receive, line, _r} ->
            if Keyword.get(options,:receive,false) do
              body = quote do
                (
                  Scheduler.permission({:receive,unquote(acc),unquote(line),[]})
                  Scheduler.stopped(:receive)
                  unquote(ast)
                )
              end
              {body,acc+1}
            else {ast,acc}
            end
          _ ->
            {ast,acc}
        end
      end)
    quote do
      unquote_splicing(offline_register)
      result =
        try do
          value = unquote(do_body)
          {:value, value}
        catch
          kind,reason -> {:exception,kind,reason,__STACKTRACE__}
        end
      unquote_splicing(offline_finish)
      case result do
        {:exception,kind,reason,stacktrace} -> :erlang.raise(kind, reason, stacktrace)
        {:value,value} -> value
      end
    end
  end

  def eval_args(args) do
    vars = Macro.generate_unique_arguments(length(args),__MODULE__)
    assignments =
      Enum.zip(args,vars)
      |> Enum.map(fn {arg,var} ->
      quote do
        unquote(var) = unquote(arg)
      end
    end)
    {assignments,vars}
  end

  defp execute_ast_with_permission(ast,offline_maybe_start) do
    quote do
      unquote_splicing(offline_maybe_start)
      try do
        result = unquote(ast)
        Scheduler.stopped(result)
        result
      catch
        kind,reason ->
          Scheduler.stopped({:exception,kind,reason})
          :erlang.raise(kind,reason,__STACKTRACE__)
      end
    end
  end
  
  defp should_instrument(modules,options,do_body) do
    Macro.postwalk(do_body, false, fn (ast,acc) ->
      should_instrument = acc ||
        case ast do
          {{:.,_line1,[{:__aliases__,_line2,[name]},_fun]},_line3,_args} ->
            Enum.member?(modules,name)
          {:send, _, [ _, _]} ->
            Keyword.get(options,:send,false)
          {:receive, _, _} ->
            Keyword.get(options,:receive,false)
          _ ->
            false
        end
      {ast,should_instrument}
    end)
  end
  
  defp configure_module(options, env) do
    if not Keyword.keyword?(options) do
      raise "options should be passed in a keyword list."
    end
    Module.put_attribute(env.module, :module, Macro.expand(env.module,env))
    
    modules = if Keyword.get(options,:modules) do
      Enum.map(options[:modules], fn module ->
        case module do
          {:__aliases__,_,[name]} -> name
          _ -> raise("incorrect specification of module in using")
        end
      end)
    else []
    end
    Module.put_attribute(env.module, :modules, modules)
    Module.put_attribute(env.module, :options, options)
  end
  
  def start_link() do
    {:ok, _} = GenServer.start_link(__MODULE__,:none,name: :scheduler)
  end
  
  def init(_) do
    {:ok,
     %{
       mode: :disabled,      # disabled, random, follow_trace
       active: nil,          # request (location) being treated
       calls: [],            # outstanding calls (with replies)
       names: [],            # names of entities
       history: [],          # executed calls, with choicepoints
       trace: [],            # trace being followed
       followed_trace: [],   # trace followed, possibly continued
       next_mode: :random,   # next mode after an interactive run
       timeout: 0
     }
    }
  end
  
  def handle_call(request,reply,state) do
    Logger.info(fn () -> "handle_call: #{inspect request} in #{inspect state.mode}" end)
    case request do
      :stop ->
        state = %{ state |
                   history: strip_finishing_alternatives(state.history),
                   followed_trace: strip_finishing_alternatives(state.followed_trace)
                 }
        {:stop,:normal,state,state}
        
        {:follow,trace} ->
        state = %{ state | mode: :follow_trace, trace: trace, next_mode: :random }
        return({:reply, :ok, state})

        {:follow_interactive,trace} ->
        state = %{ state | mode: :follow_trace, trace: trace, next_mode: :interactive }
        return({:reply, :ok, state})
        
        :interactive ->
        state = %{ state | mode: :interactive }
        return({:reply, :ok, state})
        
        :random ->
        state = %{ state | mode: :random }
        return({:reply, :ok, state})
        
        {:stopped,result} ->
        if state.active==nil do
          Logger.error("got stopped(#{inspect result}) with active=nil??")
          raise "bad"
        end
        # Check if we are following and should continue
      if state.followed_trace != [] do
        Logger.info("stopped state.followed=#{inspect hd(state.followed_trace)}")
      else
        Logger.info("stopped state.followed=[]")
      end
      if state.history != [] do
        Logger.info("stopped state.history=#{inspect hd(state.history)}")
      else
        Logger.info("stopped state.history=[]")
      end
        state = case state.followed_trace do
                  [{location,alts,old_result} | rest] ->
                    if old_result != :running do
                      Logger.warning("result #{inspect result} from #{inspect location} active=#{inspect state.active} with old result #{inspect result}")
                      end
                    %{ state | followed_trace: [{location,alts,result} | rest] }
                  [] ->
                    state
                end
        state = case state.history do
                  [{location,alts,old_result} | rest] ->
                    if old_result != :running do
                      Logger.warning("result #{inspect result} from #{inspect location} active=#{inspect state.active} with old result #{inspect result}")
                      end
                    %{ state | history: [{location,alts,result} | rest] }
                  [] ->
                    state
                end
        state = %{ state | active: nil}
        state = if state.mode == :follow_trace do
          case state.trace do
            nil -> state
            [ {first_location,_} | rest_locations ] ->
              case List.keyfind(state.calls,first_location,1) do
                nil -> state
                {reply,_} ->
                  Logger.info(fn () -> ":go_ahead to #{inspect first_location}" end)
                  GenServer.reply(reply,:go_ahead)
                  calls = List.keydelete(state.calls,first_location,1)
                  %{ state |
                     calls: calls,
                     active: first_location,
                     trace: rest_locations,
                     followed_trace: [ {first_location,[],:running} | state.followed_trace ],
                     history: [{first_location,strip_replies(calls),:running} | state.history] }
              end
          end
        else state
        end
        return({:reply, :ok, state})
            
      {:please,location} ->
        cond do
          state.mode == :disabled ->
            return({:reply, :go_ahead, state})

          # A calls is active, we just postpone other work
          state.active ->
            return({:noreply, %{ state | calls: [{reply,location}|state.calls]}})
            
            # Check if we are following and the location is the one we are looking for
          state.mode == :follow_trace ->
            case state.trace do
              [ {first_location,_} | rest_locations ] ->
                if location == first_location do
                  state = %{ state |
                             active: first_location,
                             trace: rest_locations,
                             followed_trace: [ {location,[],:running} | state.followed_trace ],
                             history: [{location,strip_replies(state.calls),:running} | state.history] }
                  Logger.info(fn () -> ":go_ahead to #{inspect first_location}; trace length #{inspect length(rest_locations)}" end)
                  return({:reply, :go_ahead, state})
                else
                  return({:noreply, %{ state | calls: [{reply,location}|state.calls]}})
                end
            end
          state.mode == :random ->
            return({:noreply, %{ state | calls: [{reply,location}|state.calls]}})
          state.mode == :interactive ->
            return({:noreply, %{ state | calls: [{reply,location}|state.calls]}})
        end
    end
  end

  def handle_info(info,state) do
    Logger.info(fn () -> "handle_info: #{inspect info} in #{inspect state.mode}; number of calls is #{inspect length(state.calls)}" end)
    case info do
      :timeout ->
        if state.timeout > 50, do: Logger.info(fn () -> "slept for #{inspect state.timeout} millseconds" end)
        cond do
          state.active ->
            return({:noreply,state})
          state.mode == :random && state.calls != [] ->
            {{reply,location},rest} = maybe_random_element(state.calls)
            history_item = {location,strip_replies(rest),:running}
            Logger.info(fn () -> "replying :go_ahead to #{inspect location}; remaining #{inspect length(rest)}" end)
            GenServer.reply(reply,:go_ahead)
            state = %{ state |
                       active: location,
                       calls: rest,
                       followed_trace: [history_item | state.followed_trace],
                       history: [history_item | state.history]
                     }
            return({:noreply, state})
          state.mode == :random && state.calls == [] ->
            return({:noreply, state})
          state.mode == :follow_trace ->
            locations = Enum.map(state.calls, fn {_,location} -> location end)
            [ {first_location,_} | _ ] = state.trace
            case List.keyfind(state.calls, first_location, 1) do
              nil -> 
                raise "Impossible to choose next trace element\n#{inspect(first_location,pretty: true)}\nsince locations are\n#{inspect(locations,pretty: true)}\ntrace: #{inspect(state.trace,pretty: true,limit: :infinity)}\nfollowed: #{inspect(Enum.reverse(state.followed_trace),pretty: true, limit: :infinity)}"
              _ ->
                raise "Internal bug"
            end
          state.mode == :interactive && state.calls != [] ->
            alternative = {reply,location} = get_alternative(state.calls)
            rest = List.delete(state.calls,alternative)
            history_item = {location,strip_replies(rest),:running}
            Logger.info(fn () -> "replying :go_ahead to #{inspect location}; remaining #{inspect length(rest)}" end)
            GenServer.reply(reply,:go_ahead)
            state = %{ state |
                       active: location,
                       calls: rest,
                       followed_trace: [history_item | state.followed_trace],
                       history: [history_item | state.history]
                     }
            return({:noreply, state})
          true -> 
            return({:noreply, state})
        end
    end
  end
  
  def get_alternative(calls) do
    IO.puts("alternatives: ")
    length_calls = length(calls)
    numbered_alternatives = Enum.zip(1..length_calls,calls)
    Enum.each(numbered_alternatives, fn {i,{_,location}} ->
      IO.puts("#{inspect i}: #{inspect location}")
    end)
    case Integer.parse(IO.gets("> ")) do
      {i, _} when i>0 and i<=length_calls ->
        {_,call} = List.keyfind(numbered_alternatives,i,0)
        call
      _ -> get_alternative(calls)
    end
  end

  defp strip_finishing_alternatives(items) do
    Enum.map(items, fn {chosen,alternatives,result} ->
      case chosen do
        {_,{:finishing,_,_,_}} -> {chosen,[],result}
        _ -> {chosen,alternatives,result}
      end
    end)
  end

  defp strip_replies(calls) do
    Enum.map(calls,fn {_,location} -> location end)
  end
  
  defp log_return(msg={:noreply, state}) do
    Logger.info(fn () -> "not setting timeout; state=#{inspect state}" end)
    msg
  end
  defp log_return(msg={:noreply, state, timeout}) do
    Logger.info(fn () -> "setting timeout #{inspect timeout}; state=#{inspect state}" end)
    msg
  end
  defp log_return(msg={:reply, reply, state}) do
    Logger.info(fn () -> "reply #{inspect reply} without timeout; state=#{inspect state}" end)
    msg
  end
  defp log_return(msg={:reply, reply, state, timeout}) do
    Logger.info(fn () -> "reply #{inspect reply} with timeout #{inspect timeout}; state=#{inspect state}" end)
    msg
  end
  
  defp return(return_spec) do
    return_spec = 
      case return_spec do
        {:reply,reply,state} ->
          state = calculate_mode(state)
          set_timeout({:reply,reply,state})
        {:noreply,state} ->
          state = calculate_mode(state)
          set_timeout({:noreply,state})
        other ->
          other
      end
    log_return(return_spec)
  end
  
  defp calculate_mode(state) do
    cond do
      (state.mode == :follow_trace) && (state.trace == []) ->
        %{ state | mode: state.next_mode }
      true ->
        state
    end
  end
  
  defp set_timeout({:noreply,state}) do
    timeout = calculate_timeout(state)
    {:noreply,%{ state | timeout: timeout},timeout}
  end
  defp set_timeout({:reply,reply,state}) do
    timeout = calculate_timeout(state)
    {:reply,reply,%{ state | timeout: timeout},timeout}
  end
  
  defp calculate_timeout(state) do
    cond do
      state.mode == :disabled ->
        :infinity
      state.active ->
        :infinity
      state.mode == :follow_trace -> 
        @timeout_following
      state.mode == :random && state.calls == [] ->
        @timeout_no_activity
      state.mode == :random ->
        @timeout_random
      state.mode == :interactive ->
        @timeout_interactive
    end
  end
  
  defp maybe_random_element(l) do
    case finishing(l) do
      nil ->
        l = Enum.sort(l)
        len = length(l)
        if len>1 do
          Logger.info(fn () -> "choosing between alternatives #{inspect Enum.map(l, fn {_,location} -> location end)}" end)
        end
        i = :rand.uniform(len)-1
        {prefix,[element|suffix]} = Enum.split(l,i)
        {element,prefix++suffix}
      other -> other
    end
  end
  
  defp finishing(l), do: finishing(l,[])
  defp finishing([],_), do: nil
  defp finishing([first|rest],l) do
    case first do
      {_,{_,{:finishing,_,_,_}}} -> {first,l++rest}
      _ -> finishing(rest,[first|l])
    end
  end

  def permission(location) do
    case Process.get(:scheduler_process_name) do
      nil ->
        raise "no name registered for process #{inspect self()} when asking for permission for #{inspect location}"
      name -> 
        GenServer.call(:scheduler,{:please,{name,location}},:infinity)
    end
  end
  
  def stopped(result) do
    GenServer.call(:scheduler,{:stopped,result},:infinity)
  end

  def register_process_name(name) do
    Logger.info(fn () -> "registering name #{inspect name} for process #{inspect self()}" end)
    Process.put(:scheduler_process_name,name)
  end
  
  def stop() do
    GenServer.call(:scheduler,:stop,:infinity)
  end
  
  def start_scheduler(scheduler) do
    case scheduler do
      {:follow,_} -> :ok
      {:follow_interactive,_} -> :ok
      :random -> :ok
      :interactive -> :ok
      _ -> raise "there is no scheduler of type #{inspect scheduler}"
    end
    GenServer.call(:scheduler,scheduler,:infinity)
  end

end

