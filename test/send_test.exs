defmodule Scheduler.Test.Send do
  use ExUnit.Case
  require Logger

  alias Scheduler.Examples.TestSend

  test "test_depth_first" do
    # Logger.put_module_level(Scheduler.Explore,:info)
    # Logger.put_module_level(Scheduler,:info)
    assert [{[alt_1,_,_,_],_},{[alt_2,_,_,_],_}] = Keyword.get(TestSend.explore_depth_first(),:results)
    assert [["1","2"],["2","1"]] = Enum.sort([alt_1,alt_2])
  end

  test "test_controlled_random" do
    # Logger.put_module_level(Scheduler.Explore,:info)
    # Logger.put_module_level(Scheduler,:info)
    assert [{[alt_1,_,_,_],_},{[alt_2,_,_,_],_}] = Keyword.get(TestSend.explore_controlled_random(),:results)
    assert [["1","2"],["2","1"]] = Enum.sort([alt_1,alt_2])
  end

  test "test_random" do
    # Logger.put_module_level(Scheduler.Explore,:info)
    # Logger.put_module_level(Scheduler,:info)
    assert [{[alt_1,_,_,_],_},{[alt_2,_,_,_],_}] = Keyword.get(TestSend.explore_random(),:results)
    assert [["1","2"],["2","1"]] = Enum.sort([alt_1,alt_2])
  end
  
end
